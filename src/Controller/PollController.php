<?php

namespace App\Controller;

use App\Entity\Answer;
use App\Entity\Poll;
use App\Form\PollType;
use App\Form\VoteType;
use App\Repository\PollRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class PollController extends AbstractController
{
    /**
     * @Route("/", name="poll_index", methods={"GET"})
     */
    public function index(PollRepository $pollRepository): Response
    {
        return $this->render('poll/index.html.twig', [
            'polls' => $pollRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="poll_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $poll = new Poll();
        $form = $this->createForm(PollType::class, $poll);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($poll);
            $entityManager->flush();

            return $this->redirectToRoute('poll_index');
        }

        return $this->render('poll/new.html.twig', [
            'poll' => $poll,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/result", name="poll_result")
     */
    public function result(Poll $poll, $id, EntityManagerInterface $entityManager): Response {
        $arrayofanswers = $entityManager->getRepository(Answer::class)->findBy([
            'poll' => $id,
        ]);
        dump($arrayofanswers);
        return $this->render('poll/results.html.twig', [
            'poll' => $poll,
            'answers' => $arrayofanswers,
        ]);
    }

    /**
     * @Route("/{id}", name="poll_show", methods={"GET","POST"})
     */
    public function show(Poll $poll, EntityManagerInterface $entityManager, Request $request, $id): Response
    {
        $pollVote = $entityManager->getRepository(Poll::class)->find($id);
        $type = $poll->getType();

        $form = $this->createForm(VoteType::class, null, [
            'content' => $pollVote->getAnswers(),
            'type' => $poll->getType(),
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            // si reponse multiples
            if($type === true) {
                $selectaws = $form['content']->getData();

                foreach ($selectaws as $selectedawr) {

                    //add +1 aux vote pour chaque question selectionnées
                    $selectedawr->setVoteCount($selectedawr->getVoteCount()+1);
                    //persist
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($selectedawr);
                }
            }
            //si reponse unique
            else {
                $selectedaws = $form['content']->getData();
                //+1 aux vote de la question choisie
                $selectedaws->setVoteCount($selectedaws->getVoteCount()+1);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($selectedaws);
            }

            $entityManager->flush();
            return $this->redirectToRoute('poll_result', [
                'id' => $poll->getId(),
                ]);
        }

        return $this->render('poll/show.html.twig', [
            'voteform' => $form->createView(),
            'type' => $type,
            'content' => $pollVote->getAnswers(),
            'poll' => $poll
        ]);
    }

    /**
     * @Route("/{id}/edit", name="poll_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Poll $poll): Response
    {
        $form = $this->createForm(PollType::class, $poll);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('poll_index');
        }

        return $this->render('poll/edit.html.twig', [
            'poll' => $poll,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="poll_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Poll $poll): Response
    {
        if ($this->isCsrfTokenValid('delete'.$poll->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($poll);
            $entityManager->flush();
        }

        return $this->redirectToRoute('poll_index');
    }
}
