import '../scss/app.scss';
import $ from 'jquery';

function addTagFormDeleteLink($tagFormLi) {
    let $removeFormButton = $('<button type="button" class="remove_button">X</button>');
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the li for the tag form
        $tagFormLi.remove();
    });
}

$(document).ready(function() {

    let $collectionHolder = $('#poll_answers');
    let $addQuestionButton = $('.add_answer_link');
    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $addQuestionButton.on('click', function(e) {
        addTagForm($collectionHolder, $addQuestionButton);
    });

    $collectionHolder.find('li').each(function() {
        addTagFormDeleteLink($(this));
    });

    addTagForm($collectionHolder, $addQuestionButton, true);
    addTagForm($collectionHolder, $addQuestionButton, true);
});

function addTagForm($collectionHolder, $newLinkLi, $start) {
        let prototype = $collectionHolder.data('prototype');

        let index = $collectionHolder.data('index');
        let newForm = prototype;

        newForm = newForm.replace(/__name__/g, index);
        $collectionHolder.data('index', index + 1);
        let $newFormLi = $('<li></li>').append(newForm);
        $newLinkLi.before($newFormLi);
        $collectionHolder.append($newFormLi);
    if(!$start) {
        addTagFormDeleteLink($newFormLi);
    }
}